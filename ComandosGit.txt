Comandos b�sicos de GIT
�git config
Uno de los comandos m�s usados en git es git config, que puede ser usado para establecer una configuraci�n espec�fica de usuario, como ser�a el caso del email, un algoritmo preferido para diff, nombre de usuario y tipo de formato, etc� Por ejemplo, el siguiente comando se usa para establecer un email:
Ejemplo:	git config --global user.email sam@google.com
�git init
Este comando se usa para crear un nuevo repertorio GIT:
Ejemplo:	git init
�git add
Este comando puede ser usado para agregar archivos al index. Por ejemplo, el siguiente comando agrega un nombre de archivo temp.txt en el directorio local del index:
Ejemplo:	git add temp.txt
�git clone
Este comando se usa con el prop�sito de revisar repertorios. Si el repertorio est� en un servidor remoto se tiene que usar el siguiente comando:
Ejemplo:	git clone alex@93.Ejemplo:88.Ejemplo:60.58:/path/to/repository
Pero si est�s por crear una copia local funcional del repertorio, usa el comando:
     git clone /path/to/repository
�git commit
El comando commit es usado para cambiar a la cabecera. Ten en cuenta que cualquier cambio comprometido no afectara al repertorio remoto. Usa el comando:
Ejemplo:	git commit �m �Message to go with the commit here�
�git status
Este comando muestra la lista de los archivos que se han cambiado junto con los archivos que est�n por ser a�adidos o comprometidos.
git status
�git push
Este es uno de los comandos m�s b�sicos. Un simple push env�a los cambios que se han hecho en la rama principal de los repertorios remotos que est�n asociados con el directorio que est� trabajando. Por ejemplo:
Ejemplo:	git push  origin master
�git checkout
El comando checkout se puede usar para crear ramas o cambiar entre ellas. Por ejemplo, el siguiente comando crea una nueva y se cambia a ella:
Ejemplo:	command git checkout -b <banch-name>

�git remote
El comando git se usa para conectar a un repositorio remoto. El siguiente comando muestra los repositorios remotos que est�n configurados actualmente:
Ejemplo:	git remote -v
Ejemplo:	git remote add origin <93.Ejemplo:88.Ejemplo:60.58>
�git pull
Para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando, el comando que se usa es:
Ejemplo:	git pull
�git tag
Etiquetar se usa para marcar commits espec�ficos con asas simples. Por ejemplo:
Ejemplo:	git tag Ejemplo:.Ejemplo:.0 <instert-commitID-here>
�git reset
Para resetear el index y el directorio que est� trabajando al �ltimo estado comprometido se usa este comando:
Ejemplo:	git reset - -hard HEAD
�git rm
Este comando se puede usar para remover archivos del index y del directorio que est� trabajando:
Ejemplo:	git rm filename.txt
�git stash
Este es uno de los comandos menos conocidos, pero ayuda a salvar cambios que no est�n por ser comprometidos inmediatamente, pero temporalmente:
Ejemplo:	git stash
�git show
Se usa para mostrar informaci�n sobre cualquier objeto git. Por ejemplo:
Ejemplo:	git show
�git fetch
Este comando le permite al usuario buscar todos los objetos de un repositorio remoto que actualmente no reside en el directorio local que est� trabajando. Por ejemplo:
Ejemplo:	git fetch origin
